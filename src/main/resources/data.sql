INSERT INTO character (character_id, fullname, alias, gender, pictureURL) VALUES (1,'Brad Pitt', 'Achilles', 'male', '');
INSERT INTO character (character_id, fullname, alias, gender, pictureURL) VALUES (2,'Johnny Depp', 'Jack Sparrow', 'male', '');
INSERT INTO character (character_id, fullname, alias, gender, pictureURL) VALUES (3,'Orlando Bloom', '', 'male', '');
INSERT INTO character (character_id, fullname, alias, gender, pictureURL) VALUES (4,'Jennifer Aniston', 'that girl from friends', 'female', '');

INSERT INTO movie (movie_id, movie_title, genre, release_year, director, pictureURL, trailerURL) VALUES (1,'Pirates of the Caribbean: 1', 'piraty', 2010, 'Jean van Paul', '', '');
INSERT INTO movie (movie_id, movie_title, genre, release_year, director, pictureURL, trailerURL) VALUES (2,'Lord of the Rings: The return of the King', 'fairytale', 2005, 'Dirk dirk', '', '');
INSERT INTO movie (movie_id, movie_title, genre, release_year, director, pictureURL, trailerURL) VALUES (3,'Inglorious Bastards', 'war', 2008, '...', '', '');
INSERT INTO movie (movie_id, movie_title, genre, release_year, director, pictureURL, trailerURL) VALUES (4,'Matrix', 'science fiction', 2001, 'Neo', '', '');
INSERT INTO movie (movie_id, movie_title, genre, release_year, director, pictureURL, trailerURL) VALUES (5,'Pirates of the Caribbean: 2', 'piraty', 2012, 'Jean van Paul', '', '');

INSERT INTO franchise (franchise_id, description, name) VALUES (1,'The Pirates are coming', 'Pirates');

