package experis.assignmentjavaspringpostgresqlapp.repository;

import experis.assignmentjavaspringpostgresqlapp.model.dao.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {}
