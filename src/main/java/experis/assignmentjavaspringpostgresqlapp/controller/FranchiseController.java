package experis.assignmentjavaspringpostgresqlapp.controller;

import experis.assignmentjavaspringpostgresqlapp.model.CommonResponse;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Character;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Franchise;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Movie;
import experis.assignmentjavaspringpostgresqlapp.repository.MovieRepository;
import experis.assignmentjavaspringpostgresqlapp.repository.FranchiseRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Franchise")
public class FranchiseController {
    private FranchiseRepository franchiseRepository;
    private MovieRepository movieRepository;

    public FranchiseController(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @GetMapping("/franchise")
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchiseData = franchiseRepository.findAll();
        return ResponseEntity.ok(franchiseData);
    }

    @GetMapping("/franchise/{id}")
    public ResponseEntity<Optional<Franchise>> getFranchiseById(@PathVariable("id") Integer id) {
        Optional<Franchise> franchise = franchiseRepository.findById(id);
        if(franchise.isPresent()){
            return ResponseEntity
                    .ok()
                    .body(franchise);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/franchise/movies/{franchiseId}")
    public ResponseEntity<List<Movie>> getAllFranchiseMovies(@PathVariable("franchiseId") Integer id) {
        List<Movie> searchResult = franchiseRepository.findById(id).get().getMovies();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult);
    }

    @GetMapping("/franchise/characters/{franchiseId}")
    public ResponseEntity<List<Character>> getAllFranchiseCharacters(@PathVariable("franchiseId") Integer id) {
        // a result list of characters in a franchise
        List<Character> franchiseCharacters = new ArrayList<>();
        Optional<Franchise> franchise = franchiseRepository.findById(id);

        if (franchise.isPresent()) {
            List<Movie> movies = franchise.get().getMovies();

            // look through every movie and find each related character
            for (int i = 0; i < movies.size(); i++) {
                List<Character> charactersInMovie = movies.get(i).getCharacters();
                // scrolling through each character, omitting character already registered within the franchise. Doing this to avoid duplicate characters
                for (int j = 0; j < charactersInMovie.size(); j++) {
                    if (!franchiseCharacters.contains(charactersInMovie.get(j))) {
                        franchiseCharacters.add(charactersInMovie.get(j));
                    }
                }
            }
        }
        return ResponseEntity.ok(franchiseCharacters);
    }

    @PatchMapping("franchise/{id}/movies")
    public ResponseEntity<CommonResponse> addMoviesToFranchise(@PathVariable("id") Integer franchiseId, @RequestBody Integer[] addingMovies) {
        Optional<Franchise> atFranchise = franchiseRepository.findById(franchiseId);
        if (atFranchise.isPresent()) {
            Franchise franchise = atFranchise.get();
            List<Movie> currentMovies = franchise.getMovies();

            for (int i = 0; i < addingMovies.length; i++) {
                Optional<Movie> atMovie = movieRepository.findById(addingMovies[i]);
                if (atMovie.isPresent()) {
                    currentMovies.add(atMovie.get());
                    atMovie.get().setFranchise(franchise);
                    movieRepository.save(atMovie.get());
                }
            }
            // set the new list with Character objects
            franchise.setMovies(currentMovies);
            // and save the updates for the movie, and the characters in a movie
            franchiseRepository.save(franchise);
            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(franchise.getMovies()));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/franchise")
    public ResponseEntity<Franchise> createFranchise(@RequestBody Franchise franchise) {
        Franchise createdFranchise = franchiseRepository.save(franchise);
        return ResponseEntity.ok(createdFranchise);
    }

    @PutMapping("/franchise/update/{id}")
    public ResponseEntity<Franchise> updateFranchise(@RequestBody Franchise franchiseUpdate, @PathVariable("id") Integer id) {
        if(!franchiseRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        franchise.setName(franchiseUpdate.getName());
        franchise.setDescription(franchiseUpdate.getDescription());

        franchiseRepository.save(franchise);
        return ResponseEntity.ok(franchise);
    }

    @DeleteMapping("/franchise/delete/{id}")
    public ResponseEntity<Boolean> deleteFranchiseById(@PathVariable("id") Integer id) {
        List<Movie> franchiseMovies = franchiseRepository.findById(id).get().getMovies();
        for (Movie movie: franchiseMovies) {
            movie.setFranchise(null);
            movieRepository.save(movie);
        }
        franchiseRepository.deleteById(id);
        return ResponseEntity.ok(true); // if this is reached it should've been a success
    }
}
