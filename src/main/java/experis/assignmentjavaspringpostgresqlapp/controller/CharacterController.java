package experis.assignmentjavaspringpostgresqlapp.controller;

import experis.assignmentjavaspringpostgresqlapp.model.CommonResponse;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Character;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Franchise;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Movie;
import experis.assignmentjavaspringpostgresqlapp.repository.CharacterRepository;
import experis.assignmentjavaspringpostgresqlapp.repository.MovieRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Character")
public class CharacterController {
    private CharacterRepository characterRepository;
    private MovieRepository movieRepository;

    public CharacterController(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    @GetMapping("/character")
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        return ResponseEntity.ok(characters);
    }

    @GetMapping("/character/{id}")
    public ResponseEntity<Optional<Character>> getCharacterById(@PathVariable("id") Integer id) {
        Optional<Character> character = characterRepository.findById(id);
        if (character.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(character);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/character/new")
    public ResponseEntity<Character> createCharacter(@RequestBody Character character) {
        Character createdCharacter = characterRepository.save(character);
        return ResponseEntity.ok(createdCharacter);
    }

    @PutMapping("/character/update/{id}")
    public ResponseEntity<CommonResponse> updateCharacter(@PathVariable("id") Integer id, @RequestParam String fullname, @RequestParam String alias, @RequestParam String gender, @RequestParam String pictureURL) {
        Optional<Character> character = characterRepository.findById(id);
        if (character.isPresent()) {
            // get the Character object, instead of Optional<Character>
            Character updatedCharacter = character.get();

            // set updated values
            updatedCharacter.setFullname(fullname);
            updatedCharacter.setAlias(alias);
            updatedCharacter.setGender(gender);
            updatedCharacter.setPictureURL(pictureURL);

            // store the updated character via the save method
            characterRepository.save(updatedCharacter);

            return ResponseEntity
                    .ok()
                    .body(new CommonResponse("update success"));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/character/delete/{id}")
    public ResponseEntity<CommonResponse> deleteCharacter(@PathVariable("id") Integer id) {
        Optional<Character> character = characterRepository.findById(id);

        if (character.isPresent()) {
            // update every field, which have relations to the movie, we are about to delete. Set update values to null, since they don't exist anymore.
            Character currentCharacter = character.get();
            List<Movie> moviesInCharacter = currentCharacter.getMovies();

            for (int i = 0; i < moviesInCharacter.size(); i++) {
                List<Character> movieCharacterList = moviesInCharacter.get(i).getCharacters();
                movieCharacterList.remove(currentCharacter);
                moviesInCharacter.get(i).setCharacters(movieCharacterList);
                movieRepository.save(moviesInCharacter.get(i));
            }

            try {
                characterRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
