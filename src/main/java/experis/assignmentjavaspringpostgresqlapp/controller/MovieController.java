package experis.assignmentjavaspringpostgresqlapp.controller;

import experis.assignmentjavaspringpostgresqlapp.model.CommonResponse;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Character;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Franchise;
import experis.assignmentjavaspringpostgresqlapp.model.dao.Movie;
import experis.assignmentjavaspringpostgresqlapp.repository.CharacterRepository;
import experis.assignmentjavaspringpostgresqlapp.repository.FranchiseRepository;
import experis.assignmentjavaspringpostgresqlapp.repository.MovieRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Movie")
public class MovieController {
    private MovieRepository movieRepository;
    private CharacterRepository characterRepository;
    private FranchiseRepository franchiseRepository;

    public MovieController(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
        this.franchiseRepository = franchiseRepository;
    }

    @GetMapping("/movie")
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity<Optional<Movie>> getMovieById(@PathVariable("id") Integer id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(movie);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/movie/characters/{movieId}")
    public ResponseEntity<List<Character>> getAllMovieCharacters(@PathVariable("movieId") Integer id) {
        Optional<Movie> movie = movieRepository.findById(id);

        if (movie.isPresent()) {
            return ResponseEntity.ok(movie.get().getCharacters());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("movie/characters/{movieId}")
    public ResponseEntity<CommonResponse> addCharactersToMovie(@PathVariable("movieId") Integer id, @RequestBody Integer[] addingCharacters) {
        Optional<Movie> atMovie = movieRepository.findById(id);
        if (atMovie.isPresent()) {
            Movie movie = atMovie.get();
            List<Character> currentCharacters = movie.getCharacters();

            for (int i = 0; i < addingCharacters.length; i++) {
                Optional<Character> atCharacter = characterRepository.findById(addingCharacters[i]);

                if (atCharacter.isPresent()) {
                    currentCharacters.add(atCharacter.get());
                }
            }

            // set the new list with Character objects
            movie.setCharacters(currentCharacters);
            // and save the updates for the movie, and the characters in a movie
            movieRepository.save(movie);

            return ResponseEntity
                    .ok()
                    .body(new CommonResponse(movie.getCharacters()));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/movie/new")
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        Movie newMovie = movieRepository.save(movie);
        return ResponseEntity.ok(newMovie);
    }

    @PutMapping("/movie/update/{id}")
    public ResponseEntity<CommonResponse> updateMovie(@PathVariable("id") Integer id, @RequestParam String movieTitle, @RequestParam String genre, @RequestParam int releaseYear, @RequestParam String director, @RequestParam String pictureURL, @RequestParam String trailerURL) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            // get the movie object, instead of Optional<movie>
            Movie updatedMovie = movie.get();

            // set updated values
            updatedMovie.setMovieTitle(movieTitle);
            updatedMovie.setGenre(genre);
            updatedMovie.setReleaseYear(releaseYear);
            updatedMovie.setDirector(director);
            updatedMovie.setPictureURL(pictureURL);
            updatedMovie.setTrailerURL(trailerURL);

            // store the updated movie via the save method
            movieRepository.save(updatedMovie);

            return ResponseEntity
                    .ok()
                    .body(new CommonResponse("update success"));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/movie/delete/{id}")
    public ResponseEntity<CommonResponse> deletemovie(@PathVariable("id") Integer id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            // update every field, which have relations to the movie, we are about to delete. Set update values to null, since they don't exist anymore.
            Movie currentMovie = movie.get();
            List<Character> charactersInMovie = currentMovie.getCharacters();
            Franchise movieFranchise = currentMovie.getFranchise();

            for (int i = 0; i < charactersInMovie.size(); i++) {
                List<Movie> characterMovieList = charactersInMovie.get(i).getMovies();
                characterMovieList.remove(currentMovie);
                charactersInMovie.get(i).setMovies(characterMovieList);
                characterRepository.save(charactersInMovie.get(i));
            }

            if (movieFranchise != null) {
                List<Movie> franchiseMovieList = movieFranchise.getMovies();
                franchiseMovieList.remove(currentMovie);
                movieFranchise.setMovies(franchiseMovieList);
                franchiseRepository.save(movieFranchise);
                currentMovie.setFranchise(null);
            }
            try {
                movieRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
