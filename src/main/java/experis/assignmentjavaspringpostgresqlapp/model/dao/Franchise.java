package experis.assignmentjavaspringpostgresqlapp.model.dao;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise
{
    @Id
    @GeneratedValue
    private Integer franchiseId;

    @OneToMany(mappedBy = "franchise")
    public List<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/movie/" + movie.getMovieId();
                    }).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @Column(nullable = false)
    private String name;

    private String description;

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Franchise() {
    }

    public Integer getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Integer franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
