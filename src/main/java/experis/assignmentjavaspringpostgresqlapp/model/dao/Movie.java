package experis.assignmentjavaspringpostgresqlapp.model.dao;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie
{
    @Id
    @GeneratedValue
    private Integer movieId;

    @JsonGetter("character")
    public List<String> charactersGetter() {
        if(characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "/character/" + character.getCharacterId();
                    }).collect(Collectors.toList());
        } else {
            return null;
        }
    }


    //@JoinTable (name = "movie_character")
    @ManyToMany (fetch = FetchType.LAZY)
    private List<Character> characters = new ArrayList<>();

    @JsonGetter("franchise")
    public String franchiseGetter() {
        if(franchise != null) {
            return "/franchise/" + franchise.getFranchiseId();
        } else {
            return null;
        }
    }

    @ManyToOne
    //@JoinColumn(name = "franchiseId")
    private Franchise franchise;

    @Column (nullable = false)
    private String movieTitle;

    @Column (nullable = false)
    private String genre;

    @Column (nullable = false)
    private int releaseYear;

    @Column (nullable = false)
    private String director;

    private String pictureURL;

    private String trailerURL;

    public Movie(List<Character> characters, Franchise franchise, String movieTitle, String genre, int releaseYear, String director, String pictureURL, String trailerURL) {
        this.characters = characters;
        this.franchise = franchise;
        this.movieTitle = movieTitle;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureURL = pictureURL;
        this.trailerURL = trailerURL;
    }

    public Movie() {
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }
}
