package experis.assignmentjavaspringpostgresqlapp.model.dao;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Character
{
    @Id
    @GeneratedValue
    private Integer characterId;

    @JsonGetter("movies")
    public List<String> moviesGetter() {
        if(movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/movie/" + movie.getMovieId();
                    }).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @ManyToMany (mappedBy = "characters", fetch = FetchType.LAZY)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Movie> movies = new ArrayList<>();

    @Column (nullable = false)
    private String fullname;

    private String alias;

    @Column (nullable = false)
    private String gender;

    private String pictureURL;

    public Character(List<Movie> movies, String fullname, String alias, String gender, String pictureURL) {
        this.fullname = fullname;
        this.alias = alias;
        this.gender = gender;
        this.pictureURL = pictureURL;
    }

    public Character() {
    }

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
