package experis.assignmentjavaspringpostgresqlapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentJavaSpringPostgreSqlAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(AssignmentJavaSpringPostgreSqlAppApplication.class, args);
    }
}
