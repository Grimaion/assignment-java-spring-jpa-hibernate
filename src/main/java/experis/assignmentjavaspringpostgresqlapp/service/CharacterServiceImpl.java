package experis.assignmentjavaspringpostgresqlapp.service;

import experis.assignmentjavaspringpostgresqlapp.model.dao.Character;
import experis.assignmentjavaspringpostgresqlapp.repository.CharacterRepository;
import org.springframework.stereotype.Service;

@Service
public class CharacterServiceImpl implements CharacterService {
    //@Autowired
    private CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository)
    {
        this.characterRepository = characterRepository;
    }

    @Override
    //@Transactional
    public Character save(Character character)
    {
        return characterRepository.save(character);
    }
}
