package experis.assignmentjavaspringpostgresqlapp.service;

import experis.assignmentjavaspringpostgresqlapp.model.dao.Character;

public interface CharacterService {
    Character save(Character character);
}
