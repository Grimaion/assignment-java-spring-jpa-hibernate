# Movie Characters API
This application entails a PostgresSQL database made with JPA & Hibernate and exposed through a web API with the use of Swagger. The database consists of franchises, movies and characters within these movies. For example Lord of The Rings, The Fellowship of The Ring, Gandalf.

<img src="https://www.lydogbillede.dk/wp-content/uploads/2015/08/minas_tirith_TOP.jpg"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html)

## Table of Contents

- [Background](#background)
- [A Primer on URI's](#aprimeronURI's)
- [Install](#install)
- [Online Deployment](#online-deployment)
- [Maintainers](#maintainers)
- [License](#license)

## Background

This Spring applicaiton is concerned with exposing the PostgreSQL database as an API using Spring. The application is deployed on Heroku.

The application consists of the following:

- A basic Rest Controller with one endpoint (`/`) which produces a JSON output.
- The Swagger documentation UI can be found at `https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html`.


## A Primer on URIs


The API endpoints for the `CharacterContoller` looks like this:
```
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Character/getAllCharacters
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Character/getCharacterById
POST https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Character/createCharacter
PUT https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Character/updateCharacter
DELETE https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Character/deleteCharacter

```
The API endpoints for the `FranchiseController` looks like this:
```
PUT https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/updateFranchise
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/getAllFranchises
POST https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/createFranchise
PATCH https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/addMoviesToFranchise
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/getFranchiseById
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/getAllFranchiseMovies
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/getAllFranchiseCharacters
DELETE https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Franchise/deleteFranchiseById


```
The API endpoints for the `MovieController` looks like this:
```
PUT https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/updateMovie
POST https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/createMovie
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/getAllMovieCharacters
PATCH https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/addCharactersToMovie
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/getAllMovies
GET https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/getMovieById
DELETE https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html#/Movie/deletemovie
```


## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Online-Deployment

A live version of this project is deployed on Heroku and can be found at https://cjj-assignment-spring-jpa.herokuapp.com/swagger-ui/index.html
## Maintainers

[Joshua Johansen (@JoshuaJohansen)](https://gitlab.com/JoshuaJohansen)

[Jakob Henriksen (@jako3417)](https://gitlab.com/Jakobah37)

[Christian Bøge-Rasmussen (@Grimaion)](https://gitlab.com/Grimaion)


## License

This project is licensed under the MIT license.

